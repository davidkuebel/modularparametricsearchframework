package uni_bayreuth.research.parametric_search.algorithms;

import java.util.Vector;

import uni_bayreuth.research.parametric_search.Oracle.OracleAnswer;
import uni_bayreuth.research.parametric_search.demo.shortcut.IMonotoneCondition;

/**
 * This class helps to filter a range between two numbers.
 * If a {@link IMonotoneCondition} is used, this is be done via binary search.
 * Note that the condition used for the minimization process has to be monotonous on the range,
 * so that binary search can be applied!
 */
public class Range<E extends Comparable<E>> {

	private Vector<E> intervalFromElementToElement;
	private E lastSatisfyingElement, lastNotSatisfyingElement, nextElement;

	public Range(E first, E last) {
		intervalFromElementToElement = new Vector<E>(2);
		if (first.compareTo(last) < 0) {
			intervalFromElementToElement.add(0, first);
			intervalFromElementToElement.add(1, last);
		} else {
			intervalFromElementToElement.add(0, last);
			intervalFromElementToElement.add(1, first);
		}
	}

	public E first() {
		if (isEmpty())
			return null;
		else
			return intervalFromElementToElement.get(0);
	}

	public void setFirst(E firstElement) {
		intervalFromElementToElement.setElementAt(firstElement, 0);
	}

	public E last() {
		if (isEmpty())
			return null;
		else
			return intervalFromElementToElement.get(1);
	}

	public void setLast(E lastElement) {
		intervalFromElementToElement.setElementAt(lastElement, 1);
	}

	public boolean isEmpty() {
		return intervalFromElementToElement.isEmpty();
	}

	public void clear() {
		intervalFromElementToElement.clear();
	}

	/**
	 * Filter the interval, so that for all elements of the interval
	 * (including first and last) the given condition is satisfied. The elements
	 * of the interval have to be naturally ordered, so that the given {@link IMonotoneCondition}
	 * is monotonically increasing or decreasing and binary search can be applied.
	 */
	public boolean filter(IMonotoneCondition<E> condition) {
		if (isEmpty())
			return true;
		OracleAnswer conditionFirst = condition.isSatisfied(first());
		switch (conditionFirst) {
			case TRUE:
				lastSatisfyingElement = first();
				return minimizeIfLastElementIsNotSatisfying(condition);
			case FALSE:
				lastNotSatisfyingElement = first();
				return minimizeIfLastElementSatisfies(condition);
			default: return false;
		}
	}

	private boolean minimizeIfLastElementIsNotSatisfying(IMonotoneCondition<E> condition) {
		OracleAnswer conditionLast = condition.isSatisfied(last());
		switch (conditionLast) {
			case TRUE: // first and last element satisfied condition
				return true;
			case FALSE:
				lastNotSatisfyingElement = last();
				boolean successful = minimizeViaBinarySearch(condition);
				if (successful)
					setLast(lastSatisfyingElement);
				return successful;
			default:
				return false;
		}
	}

	private boolean minimizeIfLastElementSatisfies(IMonotoneCondition<E> condition) {
		OracleAnswer conditionLast = condition.isSatisfied(last());
		switch (conditionLast) {
			case TRUE:
				lastSatisfyingElement = last();
				boolean successful = minimizeViaBinarySearch(condition);
				if (successful)
					setFirst(lastSatisfyingElement);
				return successful;
			case FALSE:
				clear(); // neither first nor last element satisfied condition
				return true;
			default:
				return false;
		}
	}

	private boolean minimizeViaBinarySearch(IMonotoneCondition<E> condition) {
		nextElement = condition.getMedian(lastSatisfyingElement, lastNotSatisfyingElement);
		while (! condition.abortMinimization(lastSatisfyingElement, lastNotSatisfyingElement, nextElement)) {
			OracleAnswer comparison = condition.isSatisfied(nextElement);
			switch (comparison) {
				case TRUE:
					lastSatisfyingElement = nextElement;
					break;
				case FALSE:
					lastNotSatisfyingElement = nextElement;
					break;
				default:
					return false;
			}
			nextElement = condition.getMedian(lastSatisfyingElement, lastNotSatisfyingElement);
		}
		return true;
	}
}
