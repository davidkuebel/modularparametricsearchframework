package uni_bayreuth.research.parametric_search.algorithms;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class LinearTimeSelector<E extends Comparable<E>> {

	private Comparator<E> comparator;

	public LinearTimeSelector(Comparator<E> comparator) {
		this.comparator = comparator;
	}

	/**
	 * This method selects an element of type E from a list in deterministic worst-case linear time.
	 * The index of the element should be 0 <= index < list.size().
	 * Otherwise it null is returned.
	 * 
	 * @param index
	 * @param list
	 * @return selected element
	 */
	public E select(int index, List<E> list) {
		if (index < 0 || index >= list.size())
			return null;
		E medianOfMedians = getMedianOfMedian(list);
		int maxPartitionSize = (int) Math.ceil(7/10 * list.size() + 6);
		ArrayList<E> partition1 = new ArrayList<E>(maxPartitionSize);
		ArrayList<E> partition2 = new ArrayList<E>(maxPartitionSize);
		for (E element : list) {
			if (element.compareTo(medianOfMedians) < 0)
				partition1.add(element);
			else {
				if (element != medianOfMedians)
					partition2.add(element);
			}
		}
		int k = partition1.size();
		if (index == k) {
			return medianOfMedians;
		} else if (index < k) {
			return select(index, partition1);
		} else {
			return select(index - k - 1, partition2);
		}
	}

	private E getMedianOfMedian(List<E> list) {
		if (list.size() <= 5) {
			return medianOfAtMostFive(list);
		} else {
			return medianOfMedians(list);
		}
	}

	private E medianOfAtMostFive(List<E> list) {
		if (list.isEmpty())
			return null;
		list.sort(comparator);
		int lowerMedianIndex = Math.floorDiv(list.size() - 1, 2);
		return list.get(lowerMedianIndex);
	}

	private E medianOfMedians(List<E> list) {
		int fromIndex = 0, toIndex = 4;
		ArrayList<E> medians = new ArrayList<E>(list.size()/5 + 1);
		while (fromIndex + 5 < list.size()) {
			medians.add(getMedianOfMedian(list.subList(fromIndex, toIndex + 1)));
			fromIndex += 5;
			toIndex += 5;
		}
		medians.add(getMedianOfMedian(list.subList(fromIndex, list.size())));
		return getMedianOfMedian(medians);
	}
}
