package uni_bayreuth.research.parametric_search.algorithms;

import java.util.ArrayList;

import uni_bayreuth.research.parametric_search.ComparableOracleAnswer;
import uni_bayreuth.research.parametric_search.Oracle;
import uni_bayreuth.research.parametric_search.Process;
import uni_bayreuth.research.parametric_search.Scheduler;
import uni_bayreuth.research.parametric_search.scheduler.RoundRobinScheduler;

/**
 * It implements a bitonic sorting network as described in Cormen et. al.
 * The network creates comparisons between comparable objects of type C and batches
 * these comparisons using a {@link Oracle}.
 */
public class BitonicMergesort<C extends ComparableOracleAnswer<C>> implements Process<Double>{

	private Oracle<Double> resolver;
	private Scheduler<Double> scheduler;
	private ArrayList<C> elementsToSort;
	private int sorterSize = 2; // grows from 2 to 2^k >= n

	public BitonicMergesort(ArrayList<C> elementsToSort, Oracle<Double> resolver){
		this.elementsToSort = elementsToSort;
		this.resolver = resolver;
		this.scheduler = new RoundRobinScheduler<Double>();
	}

	@Override
	public void resumeComputation() {
		// TODO Auto-generated method stub
	}

	@Override
	public Oracle<Double> getComparisonResolver() {
		// TODO Auto-generated method stub
		return null;
	}

	public ArrayList<C> sort() {
		// for value 2 do extra merge
		merge(sorterSize);
		resolver.resolveCollectedValues();
		while(sorterSize < elementsToSort.size()) {
			sorterSize*=2;
			merge(sorterSize);
			resolver.resolveCollectedValues();
			int halfCleanerSize = sorterSize/2; // reduced repeatedly from sorterSize/2 to 2
			for (; halfCleanerSize >= 2; halfCleanerSize/=2) {
				halfclean(halfCleanerSize);
				resolver.resolveCollectedValues();
			}
		}
		return elementsToSort;
	}

	private void merge(int sizeOfSingleMerger) {
		for (int i = 0; i< elementsToSort.size(); i+=sizeOfSingleMerger){
			mergeBetweenIndices(i,i+sizeOfSingleMerger-1);
		}
		scheduler.singleParallelStep();
	}

	private void halfclean(int sizeOfSingleHalfcleaner) {
		for (int i = 0; i< elementsToSort.size(); i+=sizeOfSingleHalfcleaner){
			halfcleanBetweenIndices(i,i+sizeOfSingleHalfcleaner-1);
		}
		scheduler.singleParallelStep();
	}

	private void mergeBetweenIndices(int fromIndex, int toIndex) {
		while(fromIndex < toIndex) {
			if (toIndex < elementsToSort.size()) {
				Process<Double> comparator = new BitonicMergesortComparator<C>(
						fromIndex, toIndex, elementsToSort, scheduler);
				scheduler.subscribe(comparator);
			}
			fromIndex++; toIndex--;
		}
	}

	private void halfcleanBetweenIndices(int fromIndex, int toIndex){
		int withIndex = fromIndex + (toIndex+1-fromIndex)/2;
		while(withIndex <= toIndex) {
			if (withIndex < elementsToSort.size()) {
				Process<Double> comparator = new BitonicMergesortComparator<C>(
					fromIndex, withIndex, elementsToSort, scheduler);
				scheduler.subscribe(comparator);
			}
			fromIndex++; withIndex++;
		}
	}
}
