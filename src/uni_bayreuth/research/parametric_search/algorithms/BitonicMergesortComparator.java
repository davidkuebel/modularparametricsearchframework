package uni_bayreuth.research.parametric_search.algorithms;

import java.util.ArrayList;

import uni_bayreuth.research.parametric_search.ComparableOracleAnswer;
import uni_bayreuth.research.parametric_search.Oracle;
import uni_bayreuth.research.parametric_search.Process;
import uni_bayreuth.research.parametric_search.Scheduler;

public class BitonicMergesortComparator<C extends ComparableOracleAnswer<C>> implements Process<Double> {

	private int indexCompare, indexCompareTo;
	private Scheduler<Double> scheduler;
	private ArrayList<C> elementsToSort;

	protected BitonicMergesortComparator(int indexCompare, int indexCompareTo,
			ArrayList<C> elementsToSort, Scheduler<Double> scheduler) {
		this.indexCompare = indexCompare;
		this.indexCompareTo = indexCompareTo;
		this.elementsToSort = elementsToSort;
		this.scheduler = scheduler;
	}

	@Override
	public void resumeComputation() {
		C x = elementsToSort.get(indexCompare);
		C y = elementsToSort.get(indexCompareTo);
		switch(x.isLessOrEqualTo(y)) {
			case TRUE: swapElements(x, y); break;
			case FALSE: break;
			case UNKNOWN: return; // do not unsubscribe in this case!
		}
		scheduler.unsubscribe(this);
	}

	@Override
	public Oracle<Double> getComparisonResolver() {
		throw new RuntimeException("BitonicMergesortComparator has no resolver");
	}

	private void swapElements(C x, C y) {
		elementsToSort.set(indexCompare, y);
		elementsToSort.set(indexCompareTo, x);
	}
}
