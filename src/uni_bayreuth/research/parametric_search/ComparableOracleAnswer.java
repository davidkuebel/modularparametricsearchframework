package uni_bayreuth.research.parametric_search;

import uni_bayreuth.research.parametric_search.Oracle.OracleAnswer;

public interface ComparableOracleAnswer<CompareTo>{

	OracleAnswer isLessOrEqualTo(CompareTo c);
};
