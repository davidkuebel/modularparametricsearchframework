package uni_bayreuth.research.parametric_search;

public interface Oracle<C extends Comparable<C>>{

	public enum OracleAnswer {
		TRUE,
		FALSE,
		UNKNOWN
	}

	void addCriticalValue(C value, Process<C> process);

	void resolveCollectedValues();

	OracleAnswer solve(C value, Process<C> process);

	C getLowerBoundToOptimum();
	C getUpperBoundToOptimum();

	int getNumberOfCriticalValues();
	int getNumberOfCallsOfSerialAlgorithm();
};
