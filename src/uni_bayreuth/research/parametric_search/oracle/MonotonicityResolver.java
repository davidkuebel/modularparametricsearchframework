package uni_bayreuth.research.parametric_search.oracle;

import uni_bayreuth.research.parametric_search.DecisionAlgorithm;
import uni_bayreuth.research.parametric_search.Oracle;
import uni_bayreuth.research.parametric_search.Process;

public class MonotonicityResolver<T extends Comparable<T>> implements Oracle<T> {

	private T largestKnownValueReturningFalse;
	private T smallestKnownValueReturningTrue;
	private DecisionAlgorithm<T> serialAlgorithm;
	private int numberOfCriticalValues = 0;
	private int numberOfCallsOfSerialAlgorithm = 0;

	public MonotonicityResolver(DecisionAlgorithm<T> problem, T lessThanOptimum, T greaterOrEqualOptimum) {
		this.largestKnownValueReturningFalse = lessThanOptimum;
		this.smallestKnownValueReturningTrue = greaterOrEqualOptimum;
		serialAlgorithm = problem;
	}

	@Override
	public T getLowerBoundToOptimum() {
		return smallestKnownValueReturningTrue;
	}

	@Override
	public T getUpperBoundToOptimum() {
		return largestKnownValueReturningFalse;
	}

	@Override
	public int getNumberOfCriticalValues() {
		return 	numberOfCriticalValues;
	}

	@Override
	public int getNumberOfCallsOfSerialAlgorithm() {
		return numberOfCallsOfSerialAlgorithm;
	}

	@Override
	public void addCriticalValue(T value, Process<T> process) {
		// nothing to do - values are not stored
	}

	@Override
	public void resolveCollectedValues() {
		// no values stored - nothing to resolve
	}

	/**
	 * Make sure to resolve critical values before comparing.
	 * Otherwise the comparison will definitely call {@link DecisionAlgorithm}.
	 */
	@Override
	public OracleAnswer solve(T value, Process<T> process) {
		if (value.compareTo(largestKnownValueReturningFalse) <= 0) {
			numberOfCriticalValues++;
			return OracleAnswer.FALSE;
		} else if (value.compareTo(smallestKnownValueReturningTrue) >= 0) {
			numberOfCriticalValues++;
			return OracleAnswer.TRUE;
		} else {
			solveDecisionProblemAndAdaptBound(value);
			return solve(value, process);
		}
	}

	private void solveDecisionProblemAndAdaptBound(T value) {
		numberOfCallsOfSerialAlgorithm++;
		boolean result = serialAlgorithm.solve(value);
		if (result) {
			smallestKnownValueReturningTrue = value;
		} else {
			largestKnownValueReturningFalse = value;
		}
	}
}
