package uni_bayreuth.research.parametric_search.oracle;

import java.util.ArrayList;
import java.util.Comparator;

import uni_bayreuth.research.parametric_search.DecisionAlgorithm;
import uni_bayreuth.research.parametric_search.Oracle;
import uni_bayreuth.research.parametric_search.Process;
import uni_bayreuth.research.parametric_search.algorithms.LinearTimeSelector;

public class WeightedColeResolver<T extends Comparable<T>> implements Oracle<T> {

	protected class CriticalObject implements Comparable<CriticalObject>{
		protected T criticalValue;
		protected double weight;
		protected Process<T> process;
		private CriticalObject(T criticalValue, double weight, Process<T> process) {
			this.criticalValue = criticalValue;
			this.weight = weight;
			this.process = process;
		}

		@Override
		public int compareTo(WeightedColeResolver<T>.CriticalObject o) {
			if (weight < o.weight)
				return -1;
			else if (weight > o.weight)
				return +1;
			else // weights are equal so sort by critical values
				return criticalValue.compareTo(o.criticalValue);
		}
	}

	protected class CriticalObjectWeightComparator implements Comparator<CriticalObject> {
		@Override
		public int compare(CriticalObject o1, CriticalObject o2) {
			return o1.compareTo(o2);
		}
	}

	private ArrayList<CriticalObject> criticalObjects;
	private LinearTimeSelector<CriticalObject> selector;
	private T lessThanOptimum;
	private T greaterOrEqualOptimum;
	private DecisionAlgorithm<T> serialAlgorithm;
	private double weightOfRound = Math.pow(4, 10);
	private double solvedWeightOfRound;
	private double totalWeights = 0;
	private int numberOfCriticalValues = 0;
	private int numberOfCallsOfSerialAlgorithm = 0;

	public WeightedColeResolver(DecisionAlgorithm<T> problem, T lessThanOptimum, T greaterOrEqualOptimum) {
		criticalObjects = new ArrayList<CriticalObject>();
		selector = new LinearTimeSelector<CriticalObject>(new CriticalObjectWeightComparator());
		this.lessThanOptimum = lessThanOptimum;
		this.greaterOrEqualOptimum = greaterOrEqualOptimum;
		serialAlgorithm = problem;
	}

	@Override
	public T getLowerBoundToOptimum() {
		return greaterOrEqualOptimum;
	}

	@Override
	public T getUpperBoundToOptimum() {
		return lessThanOptimum;
	}

	@Override
	public int getNumberOfCriticalValues() {
		return 	numberOfCriticalValues;
	}

	@Override
	public int getNumberOfCallsOfSerialAlgorithm() {
		return numberOfCallsOfSerialAlgorithm;
	}

	@Override
	public void addCriticalValue(T value, Process<T> process) {
		if (! isCriticalValue(value))
			return;
		else {
			CriticalObject object = new CriticalObject(value, weightOfRound, process);
			totalWeights += weightOfRound;
			criticalObjects.add(object);
		}
	}

	@Override
	public void resolveCollectedValues() {
		if (criticalObjects.isEmpty()) {
			weightOfRound = Math.pow(4, 10);
			return;
		}
		weightOfRound *= 0.25;
		double weightToSolve = totalWeights / 2;
		solvedWeightOfRound = 0;
		while (solvedWeightOfRound < weightToSolve) {
			int lowerMedianIndex = Math.floorDiv(criticalObjects.size(), 2);
			CriticalObject weightedMedian = selector.select(lowerMedianIndex, criticalObjects);
			boolean result = serialAlgorithm.solve(weightedMedian.criticalValue);
			numberOfCallsOfSerialAlgorithm++;
			if (result) {
				greaterOrEqualOptimum = weightedMedian.criticalValue;
				notifyProcesses();
			} else {
				lessThanOptimum = weightedMedian.criticalValue;
				notifyProcesses();
			}
		}
		resolveCollectedValues();
	}

	/**
	 * Make sure to resolve critical values before comparing.
	 * Otherwise the comparison will definitely call {@link DecisionAlgorithm}.
	 */
	@Override
	public OracleAnswer solve(T value, Process<T> process) {
		if (value.compareTo(lessThanOptimum) <= 0) {
			numberOfCriticalValues++;
			return OracleAnswer.FALSE;
		} else if (value.compareTo(greaterOrEqualOptimum) >= 0) {
			numberOfCriticalValues++;
			return OracleAnswer.TRUE;
		} else {
			addCriticalValue(value, process);
			return OracleAnswer.UNKNOWN;
		}
	}

	private boolean isCriticalValue(T value) {
		return (value.compareTo(lessThanOptimum) > 0)
				&& (value.compareTo(greaterOrEqualOptimum) < 0);
	}

	private void notifyProcesses() {
		CriticalObject current;
		for(int i = 0; i < criticalObjects.size(); i++) {
			current = criticalObjects.get(i);
			if ( ! isCriticalValue(current.criticalValue)) {
				criticalObjects.remove(i);
				totalWeights -= current.weight;
				solvedWeightOfRound += current.weight;
				current.process.resumeComputation();
				i--; // recall that one element has been removed
			}
		}
	}
}
