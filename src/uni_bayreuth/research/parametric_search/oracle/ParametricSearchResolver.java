package uni_bayreuth.research.parametric_search.oracle;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import uni_bayreuth.research.parametric_search.DecisionAlgorithm;
import uni_bayreuth.research.parametric_search.Oracle;
import uni_bayreuth.research.parametric_search.Process;
import uni_bayreuth.research.parametric_search.algorithms.LinearTimeSelector;

public class ParametricSearchResolver<T extends Comparable<T>> implements Oracle<T> {

    private class TComparator implements Comparator<T> {
        public int compare(T compare, T compateTo) {
            return compare.compareTo(compateTo);
        }
    }

    private List<T> criticalValues;
    private LinearTimeSelector<T> selector;
	private T lessThanOptimum;
	private T greaterOrEqualOptimum;
	private DecisionAlgorithm<T> serialAlgorithm;
	private int numberOfCriticalValues = 0;
	private int numberOfCallsOfSerialAlgorithm = 0;

	public ParametricSearchResolver(DecisionAlgorithm<T> problem, T lessThanOptimum, T greaterOrEqualOptimum) {
	    criticalValues = new ArrayList<T>();
		this.lessThanOptimum = lessThanOptimum;
		this.greaterOrEqualOptimum = greaterOrEqualOptimum;
		this.selector = new LinearTimeSelector<T>(new TComparator());
		serialAlgorithm = problem;
	}

	@Override
	public T getLowerBoundToOptimum() {
		return greaterOrEqualOptimum;
	}

	@Override
	public T getUpperBoundToOptimum() {
		return lessThanOptimum;
	}

	@Override
	public int getNumberOfCriticalValues() {
		return 	numberOfCriticalValues;
	}

	@Override
	public int getNumberOfCallsOfSerialAlgorithm() {
		return numberOfCallsOfSerialAlgorithm;
	}

    @Override
    public void addCriticalValue(T value, Process<T> process) {
        if (!isCriticalValue(value))
            return;
        else
            criticalValues.add(value);
    }

    @Override
    public void resolveCollectedValues() {
        int lowerBound = 0, upperBound = criticalValues.size() + 1;
        int next = Math.floorDiv(criticalValues.size() + 1, 2);
        while (next > lowerBound && next < upperBound ) {
            T median = selector.select(next-1, criticalValues);
            if (isCriticalValue(median)) {
                boolean result = serialAlgorithm.solve(median);
                numberOfCallsOfSerialAlgorithm++;
                if (result) {
                    greaterOrEqualOptimum = median;
                    upperBound = next;
                } else {
                    lessThanOptimum = median;
                    lowerBound = next;
                }
            } else {
                 if (median.compareTo(lessThanOptimum) > 0) // TRUE
                     upperBound = next;
                 else lowerBound = next;
            }
             next = Math.floorDiv(upperBound - lowerBound, 2) + lowerBound;
        }
        criticalValues.clear();
    }

	@Override
	public OracleAnswer solve(T value, Process<T> process) {
		if (value.compareTo(lessThanOptimum) <= 0) {
			numberOfCriticalValues++;
			return OracleAnswer.FALSE;
		} else if (value.compareTo(greaterOrEqualOptimum) >= 0) {
			numberOfCriticalValues++;
			return OracleAnswer.TRUE;
		} else {
			criticalValues.add(value);
			return OracleAnswer.UNKNOWN;
		}
	}

	private boolean isCriticalValue(T value) {
		return (value.compareTo(lessThanOptimum) > 0)
				&& (value.compareTo(greaterOrEqualOptimum) < 0);
	}
}
