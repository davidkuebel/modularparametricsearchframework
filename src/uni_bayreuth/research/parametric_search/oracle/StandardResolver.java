package uni_bayreuth.research.parametric_search.oracle;

import uni_bayreuth.research.parametric_search.DecisionAlgorithm;
import uni_bayreuth.research.parametric_search.Oracle;
import uni_bayreuth.research.parametric_search.Process;

public class StandardResolver<T extends Comparable<T>> implements Oracle<T> {

	private DecisionAlgorithm<T> serialAlgorithm;
	private T minimumSatisfyingValue;
	private T maximumNotSatisfyingValue;
	private int numberOfCallsOfSerialAlgorithm = 0;

	public StandardResolver(DecisionAlgorithm<T> problem, T aValueSmallerThanOptimum,
			T aValueGreaterThanOptimum) {
		serialAlgorithm = problem;
		this.maximumNotSatisfyingValue = aValueSmallerThanOptimum;
		this.minimumSatisfyingValue = aValueGreaterThanOptimum;
	}

	@Override
	public int getNumberOfCriticalValues() {
		return 	numberOfCallsOfSerialAlgorithm;
	}

	@Override
	public int getNumberOfCallsOfSerialAlgorithm() {
		return numberOfCallsOfSerialAlgorithm;
	}

	@Override
	public void addCriticalValue(T value, Process<T> process) {
		// do not store and sort the incoming values
	}

	@Override
	public void resolveCollectedValues() {
		// no values to collected - no values to resolve
	}

	/**
	 * The values cause an evaluation of the decision problem
	 */
	@Override
	public OracleAnswer solve(T value, Process<T> process) { // never suspend process - always deduce
		numberOfCallsOfSerialAlgorithm++;
		boolean result = serialAlgorithm.solve(value);
		if (result) {
			if (value.compareTo(minimumSatisfyingValue) < 0)
				minimumSatisfyingValue = value;
			return OracleAnswer.TRUE;
		} else {
			if (value.compareTo(maximumNotSatisfyingValue) > 0)
				maximumNotSatisfyingValue = value;
			return OracleAnswer.FALSE;
		}
	}

	@Override
	public T getLowerBoundToOptimum() {
		return minimumSatisfyingValue;
	}

	@Override
	public T getUpperBoundToOptimum() {
		return maximumNotSatisfyingValue;
	}
}
