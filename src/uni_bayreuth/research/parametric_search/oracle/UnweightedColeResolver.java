package uni_bayreuth.research.parametric_search.oracle;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import uni_bayreuth.research.parametric_search.DecisionAlgorithm;
import uni_bayreuth.research.parametric_search.Oracle;
import uni_bayreuth.research.parametric_search.Process;
import uni_bayreuth.research.parametric_search.algorithms.LinearTimeSelector;

public class UnweightedColeResolver<T extends Comparable<T>> implements Oracle<T> {

	protected class CriticalObject implements Comparable<CriticalObject>{
		protected T criticalValue;
		protected Process<T> process;
		private CriticalObject(T criticalValue, Process<T> process) {
			this.criticalValue = criticalValue;
			this.process = process;
		}

		@Override
		public int compareTo(CriticalObject o) {
			return criticalValue.compareTo(o.criticalValue);
		}
	}

	protected class CriticalObjectComparator implements Comparator<CriticalObject> {
		@Override
		public int compare(CriticalObject o1, CriticalObject o2) {
			return o1.compareTo(o2);
		}
	}

	private List<CriticalObject> criticalObjects;
	private LinearTimeSelector<CriticalObject> selector;
	private T lessThanOptimum;
	private T greaterOrEqualOptimum;
	private DecisionAlgorithm<T> serialAlgorithm;
	private int numberOfCriticalValues = 0;
	private int numberOfCallsOfSerialAlgorithm = 0;

	public UnweightedColeResolver(DecisionAlgorithm<T> problem, T lessThanOptimum, T greaterOrEqualOptimum) {
		criticalObjects = new ArrayList<CriticalObject>();
		this.lessThanOptimum = lessThanOptimum;
		this.greaterOrEqualOptimum = greaterOrEqualOptimum;
		this.selector = new LinearTimeSelector<CriticalObject>(new CriticalObjectComparator());
		serialAlgorithm = problem;
	}

	@Override
	public T getLowerBoundToOptimum() {
		return greaterOrEqualOptimum;
	}

	@Override
	public T getUpperBoundToOptimum() {
		return lessThanOptimum;
	}

	@Override
	public int getNumberOfCriticalValues() {
		return 	numberOfCriticalValues;
	}

	@Override
	public int getNumberOfCallsOfSerialAlgorithm() {
		return numberOfCallsOfSerialAlgorithm;
	}

	@Override
	public void addCriticalValue(T value, Process<T> process) {
		if (! isCriticalValue(value))
			return;
		else criticalObjects.add(new CriticalObject(value, process));
	}

	@Override
	public void resolveCollectedValues() {
		if (criticalObjects.isEmpty())
			return;

		int lowerMedianIndex = Math.floorDiv(criticalObjects.size(), 2);
		CriticalObject median = selector.select(lowerMedianIndex, criticalObjects);
		boolean result = serialAlgorithm.solve(median.criticalValue);
		numberOfCallsOfSerialAlgorithm++;

		if (result) {
			greaterOrEqualOptimum = median.criticalValue;
			notifyProcesses();
		} else {
			lessThanOptimum = median.criticalValue;
			notifyProcesses();
		}
		resolveCollectedValues();
	}

	/**
	 * Make sure to resolve critical values before comparing.
	 * Otherwise the comparison will definitely call {@link DecisionAlgorithm}.
	 */
	@Override
	public OracleAnswer solve(T value, Process<T> process) {
		if (value.compareTo(lessThanOptimum) <= 0) {
			numberOfCriticalValues++;
			return OracleAnswer.FALSE;
		} else if (value.compareTo(greaterOrEqualOptimum) >= 0) {
			numberOfCriticalValues++;
			return OracleAnswer.TRUE;
		} else {
			addCriticalValue(value, process);
			return OracleAnswer.UNKNOWN;
		}
	}

	private boolean isCriticalValue(T value) {
		return (value.compareTo(lessThanOptimum) > 0)
				&& (value.compareTo(greaterOrEqualOptimum) < 0);
	}

	private void notifyProcesses() {
		CriticalObject current;
		for(int i = 0; i < criticalObjects.size(); i++) {
			current = criticalObjects.get(i);
			if ( ! isCriticalValue(current.criticalValue)) {
				criticalObjects.remove(i);
				current.process.resumeComputation();
				i--; // one element has been removed
			}
		}
	}
}
