package uni_bayreuth.research.parametric_search;

public interface Process<C extends Comparable<C>> {
    void resumeComputation();

    Oracle<C> getComparisonResolver();
}
