package uni_bayreuth.research.parametric_search;

public interface Scheduler<C extends Comparable<C>> {
	void subscribe(Process<C> process);

	void unsubscribe(Process<C> process);

	void singleParallelStep();

	boolean hasNextParallelStep();
};
