package uni_bayreuth.research.parametric_search.scheduler;

import java.util.LinkedList;
import java.util.List;

import uni_bayreuth.research.parametric_search.Process;
import uni_bayreuth.research.parametric_search.Scheduler;

public class RoundRobinScheduler<C extends Comparable<C>> implements Scheduler<C> {

	private List<Process<C>> runningProcesses, processesToSubscribe, processesToUnsubscribe;

	public RoundRobinScheduler() {
		runningProcesses = new LinkedList<Process<C>>();
		processesToSubscribe = new LinkedList<Process<C>>();
		processesToUnsubscribe = new LinkedList<Process<C>>();
	}

	@Override
	public void subscribe(Process<C> process) {
		processesToSubscribe.add(process);
	};

	@Override
	public void unsubscribe(Process<C> process) {
		processesToUnsubscribe.add(process);
	}

	@Override
	public void singleParallelStep() {
		updateRunningProcesses();
		for (Process<C> process : runningProcesses) {
			process.resumeComputation();
		}
	}

	@Override
	public boolean hasNextParallelStep() {
		updateRunningProcesses();
		return runningProcesses.isEmpty() == false;
	}

	private void updateRunningProcesses() {
		for (Process<C> process : processesToSubscribe) {
			runningProcesses.add(process);
		}
		processesToSubscribe.clear();
		for (Process<C> process : processesToUnsubscribe) {
			runningProcesses.remove(process);
		}
		processesToUnsubscribe.clear();
	}
};
