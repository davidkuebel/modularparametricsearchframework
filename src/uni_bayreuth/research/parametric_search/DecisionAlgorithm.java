package uni_bayreuth.research.parametric_search;

public interface DecisionAlgorithm<T> {

    boolean solve(T value);

    boolean methodSolveReturnsTrueOnEquality();
};
