# README #

### What is this repository for? ###

* The repository contains core parts of a framework which supports the implementation of algorithms based on parametric search.
A full version that also contains demo applications will be released as a part of a larger framework within this year.
  
* Version: pre-release

### How do I get set up? ###

* To use the framework it is currently necessary to download the source code and copy/include these packages into your local project.

### License and Copyright ###

* The framework is released under the GNU General Public License. Please see LICENSE.txt and COPYRIGHT.txt for details.